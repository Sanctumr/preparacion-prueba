package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Ruben
 */
public class ConexionDB {
    public static Connection getConnection() {
        Connection conn = null;
        String url = "jdbc:mysql://localhost:3308/eren2019"; //Cambiar al 3306 si no funciona
        String username = "root"; //MySQL username
        String password = ""; //MySQL password   
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver"); //loading mysql driver 
            } catch (ClassNotFoundException ex) {
                System.out.println("Error Al conectar " + ex);
            }
            conn = DriverManager.getConnection(url, username, password); //attempting to connect to MySQL database
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return conn;
    }
    
    public static void close(ResultSet rs){
        try {
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }
    
    public static void close(PreparedStatement stmt){
        try {
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }
    
    public static void close(Connection conn){
        try {
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }
}
