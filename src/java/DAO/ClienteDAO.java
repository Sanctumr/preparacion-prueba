/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.ConexionDB;
import Model.Cliente;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public class ClienteDAO implements IClienteDAO {
    
    private static final String LISTA = "SELECT * FROM CLIENTE";
    private static final String INSERT = "INSERT INTO CLIENTE (nombre,apellido,direccion,correo,telefono) VALUES (?,?,?,?,?)";
    private static final String SELECT_BYID = "SELECT * FROM CLIENTE WHERE correo=?";
    private static final String DELETE = "DELETE FROM CLIENTE WHERE correo=?";
    private static final String UPDATE = "UPDATE CLIENTE SET nombre = ?, "
            + "apellido = ?, "
            + "direccion = ? ,"
            + "telefono = ?"
            + "WHERE correo = ?";

    @Override
    public ArrayList<Cliente> listCliente() {
        Connection conn = null;
        ResultSet res = null;
        PreparedStatement pst = null;

        ArrayList<Cliente> clientes = new ArrayList();
        Cliente c;
        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(LISTA);
            res = pst.executeQuery();

            while (res.next()) {
                c = new Cliente();
                c.setNombre(res.getString("nombre"));
                c.setApellido(res.getString("apellido"));
                c.setDireccion(res.getString("direccion"));
                c.setCorreo(res.getString("correo"));
                c.setTelefono(res.getString("telefono"));
                

                //llenamos el ArrayList
                clientes.add(c);
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }
        return clientes;
    }

    @Override
    public int create(Cliente c) {
        
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(INSERT);

           
            pst.setString(1, c.getNombre());
            pst.setString(2, c.getApellido());
            pst.setString(3, c.getDireccion());
            pst.setString(4, c.getCorreo());
            pst.setString(5, c.getTelefono());
            
            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public Cliente selectById(String correo) {
        
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet res = null;
        Cliente c = null;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(SELECT_BYID);

            pst.setString(1, correo);
            
            res = pst.executeQuery();

            while (res.next()) {
                c = new Cliente();
                c.setNombre(res.getString("nombre"));
                c.setApellido(res.getString("apellido"));
                c.setDireccion(res.getString("direccion"));
                c.setCorreo(res.getString("correo"));
                c.setTelefono(res.getString("telefono"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return c;
    }

    @Override
    public int update(Cliente c) {
        
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(UPDATE);

            pst.setString(1, c.getNombre());
            pst.setString(2, c.getApellido());
            pst.setString(3, c.getDireccion());
            pst.setString(4, c.getTelefono());
            pst.setString(5, c.getCorreo());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error al actualizar " + ex);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public int delete(Cliente c) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(DELETE);

            pst.setString(1, c.getCorreo());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }
    
}
