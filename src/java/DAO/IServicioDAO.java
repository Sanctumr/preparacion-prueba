/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Servicio;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public interface IServicioDAO {
   
    public ArrayList<Servicio> listServicio();
    
    public int create(Servicio s);
   
    public Servicio selectById(int id);
    
    public int update(Servicio s);
    
    public int delete(Servicio s);
}
