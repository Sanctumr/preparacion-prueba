/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.ConexionDB;
import Model.Servicio;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public class ServicioDAO implements IServicioDAO {

    private static final String LISTA = "SELECT * FROM SERVICIO";
    private static final String INSERT = "INSERT INTO SERVICIO (cod_servicio,nombre,precio) VALUES (?,?,?)";
    private static final String SELECT_BYID = "SELECT * FROM SERVICIO WHERE cod_servicio=?";
    private static final String DELETE = "DELETE FROM SERVICIO WHERE cod_servicio=?";
    private static final String UPDATE = "UPDATE SERVICIO SET nombre = ?, "
            + "precio = ? "
            + "WHERE cod_servicio = ?";

    @Override
    public ArrayList<Servicio> listServicio() {
        Connection conn = null;
        ResultSet res = null;
        PreparedStatement pst = null;

        ArrayList<Servicio> servicios = new ArrayList();
        Servicio s;
        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(LISTA);
            res = pst.executeQuery();

            while (res.next()) {
                s = new Servicio();
                s.setCodigo(res.getInt("cod_servicio"));
                s.setNombre(res.getString("nombre"));
                s.setPrecio(res.getInt("precio"));

                //llenamos el ArrayList
                servicios.add(s);
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }
        return servicios;
    }

    @Override
    public int create(Servicio s) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(INSERT);

            pst.setInt(1, s.getCodigo());
            pst.setString(2, s.getNombre());
            pst.setInt(3, s.getPrecio());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public Servicio selectById(int id) {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet res = null;
        Servicio s = null;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(SELECT_BYID);

            pst.setInt(1, id);

            res = pst.executeQuery();

            while (res.next()) {
                s = new Servicio();
                s.setCodigo(res.getInt("cod_servicio"));
                s.setNombre(res.getString("nombre"));
                s.setPrecio(res.getInt("precio"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return s;
    }

    @Override
    public int update(Servicio s) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(UPDATE);

            pst.setString(1, s.getNombre());
            pst.setInt(2, s.getPrecio());
            pst.setInt(3, s.getCodigo());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error al actualizar " + ex);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public int delete(Servicio s) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(DELETE);

            pst.setInt(1, s.getCodigo());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

}
