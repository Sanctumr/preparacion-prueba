/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;


import Model.Vehiculo;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public interface IVehiculoDAO {
    
    public ArrayList<Vehiculo> listVehiculo();
    
    public int create(Vehiculo v);
   
    public Vehiculo selectById(String patente);
    
    public int update(Vehiculo v);
    
    public int delete(Vehiculo v);
   
}
