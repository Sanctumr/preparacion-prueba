/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.ConexionDB;
import Model.Vehiculo;
import java.util.ArrayList;
import java.sql.*;

/**
 *
 * @author Ruben
 */
public class VehiculoDAO implements IVehiculoDAO {
    
    private static final String LISTA = "SELECT * FROM VEHICULO";
    private static final String INSERT = "INSERT INTO VEHICULO (patente,marca,modelo,color) VALUES (?,?,?,?)";
    private static final String SELECT_BYID = "SELECT * FROM VEHICULO WHERE patente=?";
    private static final String DELETE = "DELETE FROM VEHICULO WHERE patente=?";
    private static final String UPDATE = "UPDATE VEHICULO SET marca = ?, "
            + "modelo = ?, "
            + "color = ? "
            + "WHERE patente = ?";

    @Override
    public ArrayList<Vehiculo> listVehiculo() {
        Connection conn = null;
        ResultSet res = null;
        PreparedStatement pst = null;

        ArrayList<Vehiculo> vehiculos = new ArrayList();
        Vehiculo v;
        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(LISTA);
            res = pst.executeQuery();

            while (res.next()) {
                v = new Vehiculo();
                v.setPatente(res.getString("patente"));
                v.setMarca(res.getString("marca"));
                v.setModelo(res.getString("modelo"));
                v.setColor(res.getString("color"));
                
                
                //llenamos el ArrayList
                vehiculos.add(v);
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }
        return vehiculos;
    }

    @Override
    public int create(Vehiculo v) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(INSERT);

           
            pst.setString(1, v.getPatente());
            pst.setString(2, v.getMarca());
            pst.setString(3, v.getModelo());
            pst.setString(4, v.getColor());
            
            
            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public Vehiculo selectById(String patente) {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet res = null;
        Vehiculo v = null;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(SELECT_BYID);

            pst.setString(1, patente);

            res = pst.executeQuery();

            while (res.next()) {
                v = new Vehiculo();
                v.setPatente(res.getString("patente"));
                v.setMarca(res.getString("marca"));
                v.setModelo(res.getString("modelo"));
                v.setColor(res.getString("color"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(res);
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return v;
    }

    @Override
    public int update(Vehiculo v) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {

            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(UPDATE);

            
            pst.setString(1, v.getMarca());
            pst.setString(2, v.getModelo());
            pst.setString(3, v.getColor());
            pst.setString(4, v.getPatente());
            

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error al actualizar " + ex);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }

    @Override
    public int delete(Vehiculo v) {
        Connection conn = null;
        PreparedStatement pst = null;

        int filasAfectadas = 0;

        try {
            conn = ConexionDB.getConnection();
            pst = conn.prepareStatement(DELETE);

            pst.setString(1, v.getPatente());

            filasAfectadas = pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            ConexionDB.close(pst);
            ConexionDB.close(conn);
        }

        return filasAfectadas;
    }
    
}
