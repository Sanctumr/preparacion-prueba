/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Cliente;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public interface IClienteDAO {
    
    public ArrayList<Cliente> listCliente();
    
    public int create(Cliente c);
   
    public Cliente selectById(String correo);
    
    public int update(Cliente c);
    
    public int delete(Cliente c);
   
}
