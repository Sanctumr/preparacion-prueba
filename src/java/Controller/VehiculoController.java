/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.VehiculoDAO;
import Model.Vehiculo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ruben
 */
@WebServlet(name = "VehiculoController", urlPatterns = {"/VehiculoController"})
public class VehiculoController extends HttpServlet {

    VehiculoDAO veDAO;
    Vehiculo v;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
             String action = request.getParameter("action");
            String btnAction = request.getParameter("btnAction");
            
            //acciones GET de los botones por URL
            //muestra Vista Listado de Usuario
            if (action != null && action.equals("lista")) {
                btnAction = null;
                action = null;
                veDAO = new VehiculoDAO();
                ArrayList<Vehiculo> lstVehiculo = new ArrayList<Vehiculo>();
                lstVehiculo = veDAO.listVehiculo();
                if (lstVehiculo.size() > 0) {
                    request.setAttribute("lstVehiculo", lstVehiculo);
                    request.getRequestDispatcher("vehiculolista.jsp").forward(request, response);
                } else {
                    request.setAttribute("msg", "No existen vehiculos debe crear uno");
                    request.getRequestDispatcher("vehiculocreate.jsp").forward(request, response);
                }
                
            }
            
            ////acciones GET de los botones por URL
            //muestra Vista Creacion de Usuario
            if (action != null && action.equals("crear")) {
                response.sendRedirect("vehiculocreate.jsp");
            }
            
            
            //Acciones POST por forms boton Action Registrar Ejecuta INSERT
            if (btnAction != null && btnAction.equals("registrar")) {
                
                String patente = request.getParameter("txtPatente").trim();
                String marca = request.getParameter("txtMarca").trim();
                String modelo = request.getParameter("txtModelo").trim();
                String color = request.getParameter("txtColor").trim();
                
                v = new Vehiculo(patente, marca, modelo, color);
               
                veDAO = new VehiculoDAO();
                if (veDAO.create(v) > 0) {
                    response.sendRedirect("VehiculoController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo registrar el Vehiculo");
                    request.getRequestDispatcher("vehiculocreate.jsp").forward(request, response);
                }
            }
            
            
            
            //muestra Vista Formulario Editar Usuario
            if (action != null && action.equals("editar") && request.getParameter("id") != null) {
                v = new Vehiculo(request.getParameter("id"));
                v = veDAO.selectById(v.getPatente());
                request.setAttribute("vehiculo", v);
                request.getRequestDispatcher("vehiculo_editar.jsp").forward(request, response);
            }
            
            //Ejecuta DELETE Elimina Usuario y redirije al listado
            if (action != null && action.equals("eliminar") && request.getParameter("id") != null) {
                v = new Vehiculo(request.getParameter("id"));
                if (veDAO.delete(v) > 0) {
                    response.sendRedirect("VehiculoController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo eliminar el Vehiculo");
                    request.getRequestDispatcher("VehiculoController?action=lista").forward(request, response);
                }
            }
            
            
            
            //Acciones POST por forms boton Action Editar Ejecuta UPDATE
            if (btnAction != null && btnAction.equals("editar")) {
                
                String patente = request.getParameter("txtPatente").trim();
                String marca = request.getParameter("txtMarca").trim();
                String modelo = request.getParameter("txtModelo").trim();
                String color = request.getParameter("txtColor").trim();

                v = new Vehiculo(patente, marca, modelo, color);
                
                veDAO = new VehiculoDAO();
                if (veDAO.update(v)> 0) {
                    response.sendRedirect("VehiculoController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo actualizar el Vehiculo");
                    request.getRequestDispatcher("vehiculo_editar.jsp").forward(request, response);
                }

            } 
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
