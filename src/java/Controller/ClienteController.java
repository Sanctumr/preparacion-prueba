/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ClienteDAO;
import Model.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ruben
 */
@WebServlet(name = "ClienteController", urlPatterns = {"/ClienteController"})
public class ClienteController extends HttpServlet {
    
    ClienteDAO clDAO;
    Cliente c;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            
            String action = request.getParameter("action");
            String btnAction = request.getParameter("btnAction");
            
            //acciones GET de los botones por URL
            //muestra Vista Listado de Usuario
            if (action != null && action.equals("lista")) {
                btnAction = null;
                action = null;
                clDAO = new ClienteDAO();
                ArrayList<Cliente> lstCLiente = new ArrayList<Cliente>();
                lstCLiente = clDAO.listCliente();
                if (lstCLiente.size() > 0) {
                    request.setAttribute("lstCl", lstCLiente);
                    request.getRequestDispatcher("clientelista.jsp").forward(request, response);
                } else {
                    request.setAttribute("msg", "No existen clientes debe crear uno");
                    request.getRequestDispatcher("clientecreate.jsp").forward(request, response);
                }
                
            }
            
            ////acciones GET de los botones por URL
            //muestra Vista Creacion de Usuario
            if (action != null && action.equals("crear")) {
                response.sendRedirect("clientecreate.jsp");
            }
            
            
            //Acciones POST por forms boton Action Registrar Ejecuta INSERT
            if (btnAction != null && btnAction.equals("registrar")) {
                
                String nombre = request.getParameter("txtNombre").trim();
                String apellido = request.getParameter("txtApellido").trim();
                String direccion = request.getParameter("txtDireccion").trim();
                String correo = request.getParameter("txtCorreo").trim();
                String telefono = request.getParameter("txtTelefono").trim();
                
                c = new Cliente(nombre, apellido, direccion, correo, telefono);
               
                clDAO = new ClienteDAO();
                if (clDAO.create(c) > 0) {
                    response.sendRedirect("ClienteController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo registrar el cliente");
                    request.getRequestDispatcher("clientecreate.jsp").forward(request, response);
                }
            }
            
            
            
            //muestra Vista Formulario Editar Usuario
            if (action != null && action.equals("editar") && request.getParameter("id") != null) {
                c = new Cliente(request.getParameter("id"));
                c = clDAO.selectById(c.getCorreo());
                request.setAttribute("cliente", c);
                request.getRequestDispatcher("cliente_editar.jsp").forward(request, response);
            }
            
            //Ejecuta DELETE Elimina Usuario y redirije al listado
            if (action != null && action.equals("eliminar") && request.getParameter("id") != null) {
                c = new Cliente(request.getParameter("id"));
                if (clDAO.delete(c) > 0) {
                    response.sendRedirect("ClienteController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo eliminar el cliente");
                    request.getRequestDispatcher("ClienteController?action=lista").forward(request, response);
                }
            }
            
            
            
            //Acciones POST por forms boton Action Editar Ejecuta UPDATE
            if (btnAction != null && btnAction.equals("editar")) {
                String nombre = request.getParameter("txtNombre").trim();
                String apellido = request.getParameter("txtApellido").trim();
                String direccion = request.getParameter("txtDireccion").trim();
                String correo = request.getParameter("txtCorreo").trim();
                String telefono = request.getParameter("txtTelefono").trim();

                c = new Cliente(nombre, apellido, direccion, correo, telefono);
                
                clDAO = new ClienteDAO();
                if (clDAO.update(c)> 0) {
                    response.sendRedirect("ClienteController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo actualizar el cliente");
                    request.getRequestDispatcher("cliente_editar.jsp").forward(request, response);
                }

            } 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
