/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ServicioDAO;
import Model.Servicio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ruben
 */
@WebServlet(name = "ServicioController", urlPatterns = {"/ServicioController"})
public class ServicioController extends HttpServlet {
    
    ServicioDAO servDAO;
    Servicio s;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String action = request.getParameter("action");
            String btnAction = request.getParameter("btnAction");
            
            //acciones GET de los botones por URL
            //muestra Vista Listado de Usuario
            if (action != null && action.equals("lista")) {
                btnAction = null;
                action = null;
                servDAO = new ServicioDAO();
                ArrayList<Servicio> lstServicios = new ArrayList<Servicio>();
                lstServicios = servDAO.listServicio();
                if (lstServicios.size() > 0) {
                    request.setAttribute("lstSrv", lstServicios);
                    request.getRequestDispatcher("serviciolista.jsp").forward(request, response);
                } else {
                    request.setAttribute("msg", "No existen servicios debe crear uno");
                    request.getRequestDispatcher("serviciocreate.jsp").forward(request, response);
                }
                
            }
            
            ////acciones GET de los botones por URL
            //muestra Vista Creacion de Usuario
            if (action != null && action.equals("crear")) {
                response.sendRedirect("serviciocreate.jsp");
            }
            
            
            //Acciones POST por forms boton Action Registrar Ejecuta INSERT
            if (btnAction != null && btnAction.equals("registrar")) {
                
                int codigo = Integer.parseInt(request.getParameter("txtCodigo").trim());
                String nombre = request.getParameter("txtNombre").trim();
                int precio = Integer.parseInt(request.getParameter("txtPrecio").trim());
                
                
                s = new Servicio(codigo, nombre, precio);
               
                servDAO = new ServicioDAO();
                if (servDAO.create(s) > 0) {
                    response.sendRedirect("ServicioController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo registrar el servicio");
                    request.getRequestDispatcher("serviciocreate.jsp").forward(request, response);
                }
            }
            
            
            
            //muestra Vista Formulario Editar Usuario
            if (action != null && action.equals("editar") && request.getParameter("id") != null) {
                s = new Servicio(Integer.parseInt(request.getParameter("id").trim()));
                s = servDAO.selectById(s.getCodigo());
                request.setAttribute("servicio", s);
                request.getRequestDispatcher("servicio_editar.jsp").forward(request, response);
            }
            
            //Ejecuta DELETE Elimina Usuario y redirije al listado
            if (action != null && action.equals("eliminar") && request.getParameter("id") != null) {
                s = new Servicio(Integer.parseInt(request.getParameter("id").trim()));
                if (servDAO.delete(s) > 0) {
                    response.sendRedirect("ServicioController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo eliminar el servicio");
                    request.getRequestDispatcher("ServicioController?action=lista").forward(request, response);
                }
            }
            
            
            
            //Acciones POST por forms boton Action Editar Ejecuta UPDATE
            if (btnAction != null && btnAction.equals("editar")) {
               int codigo = Integer.parseInt(request.getParameter("txtCodigo").trim());
               String nombre = request.getParameter("txtNombre").trim();
               int precio = Integer.parseInt(request.getParameter("txtPrecio").trim());
                

               s = new Servicio(codigo, nombre, precio);
                
                servDAO = new ServicioDAO();
                if (servDAO.update(s)> 0) {
                    response.sendRedirect("ServicioController?action=lista");
                } else {
                    request.setAttribute("msg", "No se pudo actualizar el servicio");
                    request.getRequestDispatcher("servicio_editar.jsp").forward(request, response);
                }

            } 
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
