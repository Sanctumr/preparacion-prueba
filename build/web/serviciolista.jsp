<%-- 
    Document   : serviciolista
    Created on : 07-01-2021, 17:47:15
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <section id="blanca">
            <div class="container">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Administracion</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h2>Mantenedor Usuarios</h2>
                        <div class="alert alert-light" role="alert">

                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-primary" href="ServicioController?action=crear" role="button">Crear Nuevo</a>
                        <a class="btn btn-primary" href="index.jsp" role="button">Volver al Menu Crud</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Código Servicio</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${lstSrv}" var="aux">
                                <tr>
                                    <td><c:out value="${aux.getCodigo()}"/></td>
                                    <td><c:out value="${aux.getNombre()}"/></td>
                                    <td><c:out value="${aux.getPrecio()}"/></td>
                                    <td><a class="btn btn-warning" href="ServicioController?action=editar&id=${aux.getCodigo()}" role="button">Editar</a>
                                    <a class="btn btn-danger" href="ServicioController?action=eliminar&id=${aux.getCodigo()}" role="button">Eliminar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>



    <jsp:include page="footer.jsp"></jsp:include>

</body>

</html>
