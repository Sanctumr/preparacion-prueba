<%-- Document : usuariolista Created on : 06-01-2021, 13:46:35 Author : Ruben --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <section id="blanca">
            <div class="container">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Administracion</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h2>Mantenedor Usuarios</h2>
                        <div class="alert alert-light" role="alert">

                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-primary" href="ClienteController?action=crear" role="button">Crear Nuevo</a>
                        <a class="btn btn-primary" href="index.jsp" role="button">Volver al Menu Crud</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${lstCl}" var="aux">
                                <tr>
                                    <td><c:out value="${aux.getNombre()}"/></td>
                                    <td><c:out value="${aux.getApellido()}"/></td>
                                    <td><c:out value="${aux.getDireccion()}"/></td>
                                    <td><c:out value="${aux.getCorreo()}"/></td>
                                    <td><c:out value="${aux.getTelefono()}"/></td>
                                    <td><a class="btn btn-warning" href="ClienteController?action=editar&id=${aux.getCorreo()}" role="button">Editar</a>
                                    <a class="btn btn-danger" href="ClienteController?action=eliminar&id=${aux.getCorreo()}" role="button">Eliminar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>



    <jsp:include page="footer.jsp"></jsp:include>

</body>

</html>