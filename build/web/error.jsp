<%-- 
    Document   : error
    Created on : 28-12-2020, 15:18:17
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
    <h3>${msg}</h3>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
