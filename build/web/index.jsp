<%-- 
    Document   : index
    Created on : 28-12-2020, 16:44:28
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
    
    
    <section id="about">
            <div class="container">
                <div class="row justify-content-center section-title">
                    <h2 class="section-title-heading">Mantenedores</h2>
                </div>
                
                <div class="row justify-content-center section-title">
                    <h2 class="section-title-heading">Bienvenido</h2>
                </div>

                <div class="row justify-content-center text-center mt-5">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="ClienteController?action=lista">CRUD CLIENTE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="VehiculoController?action=lista">CRUD VEHICULO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ServicioController?action=lista">CRUD SERVICIO</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="ordenes">
            <div class="container mb-5">
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </section>
    
    <jsp:include page="footer.jsp"></jsp:include>
    
    </body>
</html>
