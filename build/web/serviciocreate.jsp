<%-- 
    Document   : servicoicreate
    Created on : 07-01-2021, 17:50:31
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <section id="blanca">
            <div class="container mb-10">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Administracion</h2>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-8 col-sm-12">
                        <form action="ServicioController" method="post">
                            <h2>Crear Servicios</h2>
                            <div class="alert alert-danger" role="alert">
                            ${msg}
                        </div>

                        <!--                                <div class="form-row align-items-center mb-5">
                                                            <div class="col-auto">
                                                                <input type="text" class="form-control" id="buscar" placeholder="Buscar RUT">
                                                            </div>
                                                            <div class="col-auto">
                                                                <button type="submit" class="btn btn-success">Buscar</button>
                                                            </div>
                                                        </div>-->

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtCodigo">Codigo</label>
                                    <input type="text" class="form-control" id="txtCodigo" name="txtCodigo"
                                           placeholder="1111" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtNombre">Nombre</label>
                                    <input type="text" class="form-control" id="txtNombre" name="txtNombre"
                                           placeholder="Ingrese Nombre" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtPrecio">Precio</label>
                                    <input type="text" class="form-control" id="txtPrecio" name="txtPrecio"
                                           placeholder="Ingrese Precio" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-end">
                            <button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
                            <button type="submit" class="btn btn-primary" name="btnAction"
                                    value="registrar">Registrar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </section>



    <jsp:include page="footer.jsp"></jsp:include>

</body>

</html>
