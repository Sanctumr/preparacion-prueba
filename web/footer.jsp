<footer id="footer">

    <div class="container">

        <div class="row d-flex flex-column align-items-center mt-5">

            <div class="footer-logo">
                <img src="img/logo.png" alt="">
            </div>
            <div class="social-icons">
                <a href="#"><i class="fab fa-facebook"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <div class="copyright text-center">
                <p>Copyright 2020</p>
            </div>

        </div>

    </div>

</footer>

<script defer src="js/custom.js"></script>