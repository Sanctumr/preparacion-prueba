<%-- 
    Document   : registro
    Created on : 28-12-2020, 15:17:50
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <!-- Registro -->
        <section id="ordenes">
            <div class="container">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Orden de Trabajo</h2>
                </div>
                <div class="row">
                    <div class="div col-md-12">
                        <form action="RegistroController" method="POST">
                            <h2>Datos de Cliente</h2>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtNombre">Nombre</label>
                                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" required>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtNombre">Apellido</label>
                                        <input type="text" class="form-control" id="txtApellido" name="txtApellido" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <label for="txtEmail">Email</label>
                                        <input type="text" class="form-control" id="txtEmail" name=txtEmail
                                               placeholder="cliente@gmail.com">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtDireccion">Dirección</label>
                                        <input type="text" class="form-control" id="txtDireccion" name="txtDireccion"
                                               required>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtTelefono">Teléfono</label>
                                        <input type="text" class="form-control" id="txtTelefono" name="txtTelefono"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <h2>Datos de Vehículo</h2>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtPatente">Patente</label>
                                        <input type="text" class="form-control" id="txtPatente" name="txtPatente" placeholder="XX0011"
                                               required>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtMarca">Marca</label>
                                        <input type="text" class="form-control" id="txtMarca" name="txtMarca" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <label for="txtModelo">Modelo</label>
                                            <input type="text" class="form-control" id="txtModelo" name="txtModelo"
                                                   required>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <label for="txtColor">Color</label>
                                            <input type="text" class="form-control" id="txtColor" name="txtColor" required>
                                        </div>
                                    </div>
                                </div>

                                <h2>Servicio a prestar</h2>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 mb-2">
                                            <label for="cboEncargado">Encargado</label>
                                            <select class="form-control" id="cboEncargado" name="cboEncargado">
                                            <option value="0" selected disabled>Seleccione...</option>
                                            <c:forEach items="${lstEncargados}" var="enc">    
                                                <option value="${enc.id}">${enc.nombre}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Servicio</th>
                                                <th>Precio</th>
                                                <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        <c:set var="i" value="1"></c:set>
                                        <c:forEach items="${lstServicios}" var="srv">
                                            <tr>
                                                <td>${srv.getNombre()}</td>
                                                <td>${srv.getPrecio()}</td>
                                                <td><input type="number" class="form-control" id="txtCantidad${i}" name="txtCantidad${i}" value="0" required></td>
                                                <input type="hidden" name="txtServicio${i}" value="${i}">
                                            </tr>     
                                            <c:set var="i" value="${i+1}"></c:set>
                                        </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
                                <button type="submit" class="btn btn-primary" name="btnAceptar">Registrar</button>
                            </div>
                    </form>
                </div>
            </div>
    </section>


    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>