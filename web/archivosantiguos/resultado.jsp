<%-- 
    Document   : resultado
    Created on : 28-12-2020, 15:18:02
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>



        <section id="ordenes">
            <div class="container">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Resultado Orden</h2>
                </div>
                <div class="row">
                    <div class="div col-md-12">
                    <c:forEach items="${lstOrdenes}" var="aux">
                        <p>${aux.getCliente().getNombre()}</p>
                        <p>${aux.getVehiculo().getPatente()}</p>
                        <p>${aux.getEncargado().getNombre()}</p>
                        <p>${aux.getServicios()[0].getNombre()}</p>
                        <p>${aux.getServicios()[0].getPrecio()} x ${cant1}</p>
                        <p>${aux.getServicios()[1].getNombre()}</p>
                        <p>${aux.getServicios()[1].getPrecio()} x ${cant2}</p>
                        <p>${aux.getServicios()[2].getNombre()}</p>
                        <p>${aux.getServicios()[2].getPrecio()} x ${cant3}</p>

                    </c:forEach>
                    <h3>$${total}</h3>

                </div>
            </div>
    </section>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>