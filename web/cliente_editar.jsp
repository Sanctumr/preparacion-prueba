<%-- 
    Document   : clienteeditar
    Created on : 07-01-2021, 16:58:13
    Author     : Ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <section id="blanca">
            <div class="container mb-10">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Administracion</h2>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-8 col-sm-12">
                        <form action="ClienteController" method="post">
                            <h2>Mantenedor Cliente</h2>
                            <div class="alert alert-danger" role="alert">
                            ${msg}
                        </div>

                        <!--                                <div class="form-row align-items-center mb-5">
                                                            <div class="col-auto">
                                                                <input type="text" class="form-control" id="buscar" placeholder="Buscar RUT">
                                                            </div>
                                                            <div class="col-auto">
                                                                <button type="submit" class="btn btn-success">Buscar</button>
                                                            </div>
                                                        </div>-->


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtNombre">Nombre</label>
                                    <input type="text" class="form-control" id="txtNombre" name="txtNombre"
                                           value="${cliente.nombre}">
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtApellido">Apellido</label>
                                    <input type="text" class="form-control" id="txtApellido" name="txtApellido"
                                           value="${cliente.apellido}">
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtDireccion">Direccion</label>
                                    <input type="text" class="form-control" id="txtDireccion" name="txtDireccion"
                                           value="${cliente.direccion}">
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtCorreo">Correo</label>
                                    <input type="text" class="form-control" id="txtCorreo" name=""
                                           value="${cliente.correo}" disabled>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtTelefono">Telefono</label>
                                    <input type="text" class="form-control" id="txtTelefono" name="txtTelefono"
                                           value="${cliente.telefono}">
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-end">
                            <input type="hidden" name="txtCorreo" value="${cliente.correo}">
                            <button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
                            <button type="submit" class="btn btn-primary" name="btnAction"
                                    value="editar">Actualizar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </section>



    <jsp:include page="footer.jsp"></jsp:include>

</body>

</html>
